<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitation extends Model
{
    protected $fillable = [
        'solicitation',
    ];

    protected $casts = [
        'solicitation' => 'array',
    ];
}
