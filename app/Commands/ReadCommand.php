<?php

namespace App\Commands;

use App\Solicitation;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use Illuminate\Support\Facades\File;
use Storage;

class ReadCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'read {file : Enter file name}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Read the selected file (ex: logs.txt) and save the information in the database';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->dropTableData();

        $logs = Storage::get('storage/log/'.$this->argument('file'));

        foreach (explode("\n", $logs) as $rawSolicitation) {

            $solicitation = json_decode($rawSolicitation,true);

            if ($solicitation) {
                $array[] = $solicitation;
            }
            if (isset($array[1000])) {
                $this->storeLogs($array);

                $array = [];
            }            
        }
        $this->storeLogs($array);
    }

    /**
     * Store logs in the database
     *
     * @param Array $array
     * @return void
     */
    public function storeLogs(Array $array)
    {
        Solicitation::create([
            'solicitation' => json_encode($array),
        ]);
    }

    /**
     * Drop the data on the table solicitations
     *
     * @return void
     */
    public function dropTableData()
    {
        Solicitation::truncate();
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
