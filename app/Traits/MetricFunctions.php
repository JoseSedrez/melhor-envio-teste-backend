<?php

namespace App\Traits;

use App\Solicitation;
use Storage;

trait MetricFunctions {

    /**
     * Write a CSV file with the proxy, kong and request averages by service
     *
     * @param Array $proxy
     * @param Array $kong
     * @param Array $request
     * @return void
     */
    public function writeLatenciesAverage(Array $serviceIds, Array $latencies)
    {
        Storage::put('storage/metrics/LatenciesAverage.csv','');
        $fp = fopen('storage/metrics/LatenciesAverage.csv', 'wb');
        fputcsv($fp, ['Services','Proxy Average','Kong Average','Request Average']);

        foreach (array_unique($serviceIds) as $serviceId) {
            foreach ($latencies as $latencie) {
                if ($latencie['service'] == $serviceId) {
                    $proxy[] = $latencie['proxy'];
                    $kong[] = $latencie['kong'];
                    $request[] = $latencie['request'];
                }
            }
            fputcsv($fp, [
                $serviceId,
                $this->getAverageOfArray($proxy),
                $this->getAverageOfArray($kong),
                $this->getAverageOfArray($request)
                ]);
        }
        fclose($fp);
    }

    /**
     * Return tha average of the values in an array
     *
     * @param Array $array
     * @return float
     */
    public function getAverageOfArray(Array $array)
    {
        $average = (float) array_sum($array)/count($array);
        return number_format($average,2);
    }

    /**
     * Write a CSV file with the requests by services
     *
     * @param [Array] $arrayServicesId
     * @return void
     */
    public function writeRequestByService(Array $serviceIds)
    {   
        Storage::put('storage/metrics/RequestByService.csv','');
        
        $fp = fopen('storage/metrics/RequestByService.csv', 'wb');

        fputcsv($fp, ['Services','Requests by services']);
        foreach ($this->countArrayValues($serviceIds) as $key => $value) {
            fputcsv($fp, [$key,$value]);
        }

        fclose($fp);
    }

    /**
     * Return a array with the param values as key and the count of param values as value
     *
     * @param Array $array
     * @return Array
     */
    public function countArrayValues(Array $array)
    {
        return (Array) array_count_values($array);
    }

    /**
     * Write a CSV file with the requests by consumers
     *
     * @param [Array] $arrayConsumersId
     * @return void
     */
    public function writeRequestByConsumer(Array $consumerIds)
    {   
        Storage::put('storage/metrics/RequestByConsumer.csv','');
        
        $fp = fopen('storage/metrics/RequestByConsumer.csv', 'wb');

        fputcsv($fp, ['Consumers','Requests by consumers']);
        foreach ($this->countArrayValues($consumerIds) as $key => $value) {
            fputcsv($fp, [$key,$value]);
        }

        fclose($fp);
    }
}