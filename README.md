# Instalação

Para ser capaz de utilizar a aplicação você deve primeiramente clonar o repositório utilizando o seguinte comando no terminal:

`git clone git@bitbucket.org:JoseSedrez/melhor-envio-teste-backend.git`

Após clonar o repositório execute o seguinte comando:

`composer install`

## Banco de dados

Para que a aplicação funcione é preciso que o banco de dados seja configurado no arquivo "config/database.php". Atualmente o banco está configurado da seguinte forma:

```
'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '172.18.0.10'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'me_teste_backend'),
            'username' => env('DB_USERNAME', 'root'),
            'password' => env('DB_PASSWORD', 'mysql'),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],
```

# Modo de uso

A aplicação consiste em ler um arquivo, salvar seus dados no banco e então gerar relatórios de métricas em forma de CSV.

As métricas consistem em:

- Requisições por consumidor
- Requisições por serviço
- Tempo médio de request, proxy e kong por serviço

## Leitura

O arquivo com as informações que serão lidas deve ser colocado dentro do diretório "storage/log", para ser mais prático o arquivo "logs.txt" já está lá, mas caso queira ler outro arquivo com dados no mesmo padrão do arquivo "logs.txt" o mesmo deve ser colocado no diretório indicado.

Para ler o arquivo execute o seguinte comando:

`php me-teste-backend read {nome do arquivo}`

No caso, para ler o arquivo "logs.txt" deve-se executar:

`php me-teste-backend read logs.txt`

Os dados dentro do arquivo lido serão armazenados dentro do banco de dados.

## Geração de métricas

Para gerar as métricas o seguinte comando deve ser executado:

`php me-teste-backend read {tipo de métrica}`

Os tipos de métrica são as seguintes:

- "LatenciesAverage" : Gera um CSV de mesmo nome com a média de tempo de request, proxy e kong por serviço
- "RequestByConsumer" : Gera um CSV de mesmo nome com a quantidade de requisições por consumidor
- "RequestByService" : Gera um CSV de mesmo nome com a quantidade de requisições por serviço
- "AllMetrics" : Gera 3 CSVs com as métricas citadas anteriormente

Os CSVs gerados com as métricas estarão disponíveis para leitura no diretório "storage/metrics".

# E-mail de contato

jose.sedrez@melhorenvio.com