<?php

namespace App\Commands;

use App\Solicitation;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use App\Traits\MetricFunctions;
use Storage;

class WriteCommand extends Command
{
    use MetricFunctions;
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'write {metric : Enter the metric type}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Write a CSV file with metrics about the data on database';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $allSolicitations = Solicitation::all()->toArray();
        $metric = $this->argument('metric');

        foreach ($allSolicitations as $solicitations) {
            foreach (json_decode($solicitations['solicitation'],true) as $solicitation) {
                $consumerIds[] = $solicitation['authenticated_entity']['consumer_id']['uuid'];
                $serviceIds[] = $solicitation['service']['id'];
                $latencies[] = [
                    'service' => $solicitation['service']['id'],
                    'proxy' => $solicitation['latencies']['proxy'],
                    'kong' => $solicitation['latencies']['kong'],
                    'request' => $solicitation['latencies']['request']
                ];
            }   
        }
        
        if ($metric == 'LatenciesAverage' || $metric == 'AllMetrics') {
            $this->writeLatenciesAverage($serviceIds, $latencies);
        }

        if ($metric == 'RequestByConsumer' || $metric == 'AllMetrics') {
            $this->writeRequestByConsumer($consumerIds);
        }

        if ($metric == 'RequestByService' || $metric == 'AllMetrics') {
            $this->writeRequestByService($serviceIds);
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
